package commands

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/pokebot/pkg/messagecodes"
	"strings"
)

func (cmd *Commander) English(events *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message)

	go func() {
		defer close(out)
		if events.Transport != transport.CHANNEL {
			return
		}

		for _, event := range events.RawEvents {
			senderName := strings.ToLower(event.Sender.Name)
			// Extract name
			split := strings.Split(event.Message, " ")
			if len(split) < 2 {
				out <- &messages.Message{
					Code:      messagecodes.MissingParam,
					Transport: transport.CHANNEL,
					Target:    events.Target,
					Message:   fmt.Sprintf("@%s missing pokemon name after !type-command", event.Sender.Name),
				}
				continue
			}
			german := strings.ToLower(split[1])

			// Translate
			translation, exists := cmd.Translator.deToEn[german]
			if exists {
				out <- &messages.Message{
					Code:      messagecodes.Translation,
					Transport: transport.CHANNEL,
					Message:   fmt.Sprintf("@%s de: '%s' en: '%s'", senderName, german, translation),
					Target:    event.Target,
				}
			} else {
				out <- &messages.Message{
					Code:      messagecodes.Translation,
					Transport: transport.CHANNEL,
					Message:   fmt.Sprintf("@%s no pokemon found with german name '%s'", senderName, german),
					Target:    event.Target,
				}
			}
		}
	}()

	return out
}
