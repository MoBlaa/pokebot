package commands

import (
	"encoding/json"
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/pokebot/pkg/domain"
	"gitlab.com/MoBlaa/pokebot/pkg/messagecodes"
	"gitlab.com/MoBlaa/pokebot/pkg/typings"
	"net/http"
	"strings"
)

type PokeApiV2Type struct {
	Name string `json:"name"`
}

type PokeApiV2Types struct {
	Slot int           `json:"slot"`
	Type PokeApiV2Type `json:"type"`
}

type PokeApiV2Response struct {
	Types []PokeApiV2Types `json:"types"`
}

func (cmd *Commander) Types(events *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message)

	go func() {
		defer close(out)
		// Check for valid event
		if events.Transport != transport.CHANNEL {
			// TODO: Error handling
			return
		}

		// Make requests for all pokemon requested
		for _, event := range events.RawEvents {
			// Extract name
			split := strings.Split(event.Message, " ")
			if len(split) < 2 {
				out <- &messages.Message{
					Code:      messagecodes.MissingParam,
					Transport: transport.CHANNEL,
					Target:    events.Target,
					Message:   fmt.Sprintf("@%s missing pokemon name after !type-command", event.Sender.Name),
				}
				continue
			}
			name := strings.ToLower(split[1])

			translation, exists := cmd.Translator.DeToEn(name)

			// Try with name
			var types *domain.Pokemon
			var err error
			if exists {
				types, err = fetchPokemonTypes(translation)
			} else {
				types, err = fetchPokemonTypes(name)
			}
			if err != nil {
				out <- &messages.Message{
					Code:      messagecodes.NotFound,
					Transport: transport.CHANNEL,
					Target:    events.Target,
					Message:   err.Error(),
				}
				continue
			}

			out <- &messages.Message{
				Code:      messagecodes.PokemonTypes,
				Transport: transport.CHANNEL,
				Target:    events.Target,
				Message:   fmt.Sprintf("@%s %s has types %s", event.Sender.Name, name, join(types.Types)),
			}
		}
	}()
	return out
}

func fetchPokemonTypes(name string) (*domain.Pokemon, error) {
	rsp, err := http.Get(fmt.Sprintf("https://pokeapi.co/api/v2/pokemon/%s", name))
	if err != nil {
		return nil, fmt.Errorf("failed to fetch information about '%s'", name)
	}
	if rsp.StatusCode == http.StatusNotFound {
		return nil, fmt.Errorf("no pokemon with name '%s' found", name)
	} else if rsp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("invalid response code for '%s': %v", name, rsp.StatusCode)
	}

	// Parse response
	var response PokeApiV2Response
	err = json.NewDecoder(rsp.Body).Decode(&response)
	if err != nil {
		return nil, fmt.Errorf("failed to parse response: %v", err)
	}

	// Build Printable response
	types := &domain.Pokemon{Name: name}
	for _, typeSlot := range response.Types {
		types.Types = append(types.Types, typings.PokeType(typeSlot.Type.Name))
	}
	return types, nil
}
