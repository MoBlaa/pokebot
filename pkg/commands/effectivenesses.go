package commands

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/pokebot/pkg/domain"
	"gitlab.com/MoBlaa/pokebot/pkg/messagecodes"
	"gitlab.com/MoBlaa/pokebot/pkg/typings"
	"strings"
)

func (cmd *Commander) Effects(events *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message)

	go func() {
		defer close(out)
		// Check for valid event
		if events.Transport != transport.CHANNEL {
			// TODO: Error handling
			return
		}

		// Make requests for all pokemon requested
		for _, event := range events.RawEvents {
			senderName := strings.ToLower(event.Sender.Name)
			// Extract name
			split := strings.Split(event.Message, " ")
			if len(split) < 2 {
				out <- &messages.Message{
					Code:      messagecodes.MissingParam,
					Transport: transport.CHANNEL,
					Target:    events.Target,
					Message:   fmt.Sprintf("@%s missing pokemon name after !type-command", event.Sender.Name),
				}
				continue
			}
			name := strings.ToLower(split[1])

			translation, exists := cmd.Translator.DeToEn(name)

			// Try with name
			var types *domain.Pokemon
			var err error
			if exists {
				types, err = fetchPokemonTypes(translation)
			} else {
				types, err = fetchPokemonTypes(name)
			}
			if err != nil {
				out <- &messages.Message{
					Code:      messagecodes.NotFound,
					Transport: transport.CHANNEL,
					Target:    events.Target,
					Message:   err.Error(),
				}
				continue
			}

			// Get effectivenesses
			var effects *typings.Effects
			if len(types.Types) >= 2 {
				effects = typings.Effectivenesses(types.Types[0], types.Types[1])
			} else {
				effects = typings.Effectivenesses(types.Types[0])
			}
			out <- &messages.Message{
				Code:      messagecodes.Effectivenesses,
				Transport: transport.CHANNEL,
				Target:    events.Target,
				Message:   fmt.Sprintf("@%s attacks on %s have NO: %s, 1/4x: %s, 1/2x: %s, 2x: %s, 4x: %s effects", senderName, name, join(effects.No), join(effects.Quarter), join(effects.Half), join(effects.Double), join(effects.Quadruple)),
			}
		}
	}()
	return out
}
