package commands

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/pokebot/pkg/messagecodes"
	"gitlab.com/MoBlaa/pokebot/pkg/typings"
	"strings"
)

func join(str []typings.PokeType) string {
	var result strings.Builder
	result.WriteString("[ ")
	for i, s := range str {
		result.WriteString(string(s))
		if i < len(str)-1 {
			result.WriteString(", ")
		}
	}
	result.WriteString(" ]")
	return result.String()
}

// Weakness returns the weaknesses of some type
func Weakness(events *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message)

	go func() {
		defer close(out)
		// Check for valid event
		if events.Transport != transport.CHANNEL {
			// TODO: Error handling
			return
		}

		// Make requests for all pokemon requested
		for _, event := range events.RawEvents {
			senderName := strings.ToLower(event.Sender.Name)

			// Extract name
			split := strings.Split(event.Message, " ")
			if len(split) < 2 {
				out <- &messages.Message{
					Code:      messagecodes.MissingParam,
					Transport: transport.CHANNEL,
					Target:    events.Target,
					Message:   fmt.Sprintf("@%s missing pokemon name after !type-command", event.Sender.Name),
				}
				continue
			}
			typeName := strings.ToLower(split[1])

			// Return types
			weaknesses, ok := typings.Effectiv[typings.PokeType(typeName)]
			if ok {
				out <- &messages.Message{
					Code:      messagecodes.TypeWeaknesses,
					Transport: transport.CHANNEL,
					Message:   fmt.Sprintf("@%s weaknesses of '%s' are: %s", senderName, typeName, join(weaknesses)),
					Target:    event.Target,
				}
			} else {
				out <- &messages.Message{
					Code:      messagecodes.TypeWeaknesses,
					Transport: transport.CHANNEL,
					Message:   fmt.Sprintf("@%s type %s not found", senderName, typeName),
					Target:    event.Target,
				}
			}
		}
	}()
	return out
}
