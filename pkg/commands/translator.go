package commands

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

type Translator struct {
	enToDe map[string]string
	deToEn map[string]string
}

func Load(path string) (*Translator, error) {
	translator := &Translator{
		enToDe: make(map[string]string),
		deToEn: make(map[string]string),
	}
	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)
	// Parse to en-to-de map
	var result map[string]string
	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		return nil, err
	}
	// Add to Translator
	for key, val := range result {
		key = strings.ToLower(key)
		val = strings.ToLower(val)
		translator.enToDe[key] = val
		translator.deToEn[val] = key
	}
	return translator, err
}

func (t *Translator) DeToEn(de string) (translation string, exists bool) {
	translation, exists = t.deToEn[strings.ToLower(de)]
	return
}

func (t *Translator) EnToDe(en string) (translation string, exists bool) {
	translation, exists = t.enToDe[strings.ToLower(en)]
	return
}
