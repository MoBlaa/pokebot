package typings

type PokeType string

const (
	Normal   PokeType = "normal"
	Fight    PokeType = "fight"
	Flying   PokeType = "flying"
	Poison   PokeType = "poison"
	Ground   PokeType = "ground"
	Rock     PokeType = "rock"
	Bug      PokeType = "bug"
	Ghost    PokeType = "ghost"
	Steel    PokeType = "steel"
	Fire     PokeType = "fire"
	Water    PokeType = "water"
	Grass    PokeType = "grass"
	Electric PokeType = "electric"
	Psychic  PokeType = "psychic"
	Ice      PokeType = "ice"
	Dragon   PokeType = "dragon"
	Dark     PokeType = "dark"
	Fairy    PokeType = "fairy"
)

var (
	// Key: Defense, Value: Attack
	NoEffect = map[PokeType][]PokeType{
		Normal: {Ghost},
		Flying: {Ground},
		Ground: {Electric},
		Ghost:  {Normal, Fight},
		Steel:  {Poison},
		Dark:   {Psychic},
		Fairy:  {Dragon},
	}
	// Key: Defense, Value: Attack
	HalfEffect = map[PokeType][]PokeType{
		Fight:    {Rock, Bug, Dark},
		Flying:   {Fight, Bug, Grass},
		Poison:   {Fight, Poison, Bug, Grass, Fairy},
		Ground:   {Poison, Rock},
		Rock:     {Normal, Flying, Poison, Fire},
		Bug:      {Fight, Ground, Grass},
		Ghost:    {Poison, Bug},
		Steel:    {Normal, Flying, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy},
		Fire:     {Bug, Steel, Fire, Grass, Ice, Fairy},
		Water:    {Steel, Fire, Water, Ice},
		Grass:    {Ground, Water, Grass, Electric},
		Electric: {Flying, Steel, Electric},
		Psychic:  {Fight, Psychic},
		Ice:      {Ice},
		Dragon:   {Fire, Water, Grass, Electric},
		Dark:     {Ghost, Dark},
		Fairy:    {Fight, Bug, Dark},
	}
	// Key: Defense, Value: Attack
	Effectiv = map[PokeType][]PokeType{
		Normal:   {Fight},
		Fight:    {Flying, Psychic, Fairy},
		Flying:   {Rock, Electric, Ice},
		Poison:   {Ground, Psychic},
		Ground:   {Water, Grass, Ice},
		Rock:     {Fight, Ground, Steel, Water, Grass},
		Bug:      {Flying, Rock, Fire},
		Ghost:    {Ghost, Dark},
		Steel:    {Fight, Ground, Fire},
		Fire:     {Ground, Rock, Water},
		Water:    {Grass, Electric},
		Grass:    {Flying, Poison, Bug, Fire, Ice},
		Electric: {Ground},
		Psychic:  {Bug, Ghost, Dark},
		Ice:      {Fight, Rock, Steel, Fire},
		Dragon:   {Ice, Dragon, Fairy},
		Dark:     {Fight, Bug, Fairy},
		Fairy:    {Poison, Steel},
	}
)

type Effects struct {
	No        []PokeType
	Quarter   []PokeType
	Half      []PokeType
	Double    []PokeType
	Quadruple []PokeType
}

func Effectivenesses(types ...PokeType) *Effects {
	// Build map for all types
	m := make(map[PokeType]float32)

	// Add no effectives
	var noEffect []PokeType
	var halfEffect []PokeType
	var effectiv []PokeType
	for _, t := range types {
		noEffect = append(noEffect, NoEffect[t]...)
		halfEffect = append(halfEffect, HalfEffect[t]...)
		effectiv = append(effectiv, Effectiv[t]...)
	}
	for _, entry := range noEffect {
		m[entry] = 0
	}

	// Add half effectives
	for _, entry := range halfEffect {
		tmp, present := m[entry]
		if !present {
			tmp = 1.0
		}
		m[entry] = tmp * 1 / 2
	}

	// Add Double effectives
	for _, entry := range effectiv {
		tmp, present := m[entry]
		if !present {
			tmp = 1.0
		}
		m[entry] = tmp * 2
	}

	eff := &Effects{}
	for key, val := range m {
		if val == 0 {
			eff.No = append(eff.No, key)
		}
		if val == 1.0/4.0 {
			eff.Quarter = append(eff.Quarter, key)
		}
		if val == 1.0/2.0 {
			eff.Half = append(eff.Half, key)
		}
		if val == 2 {
			eff.Double = append(eff.Double, key)
		}
		if val == 4 {
			eff.Quadruple = append(eff.Quadruple, key)
		}
	}
	return eff
}
