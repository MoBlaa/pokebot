package domain

import (
	"fmt"
	"gitlab.com/MoBlaa/pokebot/pkg/typings"
)

type Pokemon struct {
	Name  string
	Types []typings.PokeType
}

func (types *Pokemon) String() string {
	return fmt.Sprintf("%s: %s", types.Name, types.Types)
}
