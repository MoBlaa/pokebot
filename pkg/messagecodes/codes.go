package messagecodes

import "gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"

const (
	InvalidTransport messagecode.Code = 30
	MissingParam     messagecode.Code = 31
	NotFound         messagecode.Code = 32
	PokemonTypes     messagecode.Code = 269
	TypeWeaknesses   messagecode.Code = 270
	Translation      messagecode.Code = 271
	Effectivenesses  messagecode.Code = 272
)
