package main

import (
	"encoding/json"
	"gitlab.com/MoBlaa/bot-chassis/pkg"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/timeouts"
	"gitlab.com/MoBlaa/pokebot/pkg/commands"
	"log"
	"path/filepath"
)

func main() {
	cfg, err := config.Load("PBOT_", filepath.FromSlash("./configs/.env"))
	if err != nil {
		log.Fatal(err)
	}

	tom := timeouts.New()
	defer tom.Close()

	b := pkg.New(*cfg, func(data []byte) (*config.Config, error) {
		var cfg config.Config
		err := json.Unmarshal(data, &cfg)
		return &cfg, err
	})

	blank := func(event *messages.BundledEvent) <-chan *messages.Message {
		ch := make(chan *messages.Message)
		defer close(ch)
		return ch
	}

	translator, err := commands.Load(filepath.FromSlash("./configs/pokemon_names_entode.json"))
	if err != nil {
		log.Fatal("error reading pokemon name translations", err)
	}

	commander := &commands.Commander{
		Translator: translator,
	}

	b.AddCommand("!type", commander.Types)
	b.AddCommand("!weakness", commands.Weakness)
	b.AddCommand("!en", commander.English)
	b.AddCommand("!eff", commander.Effects)

	// Overwrite commands to ignore
	b.AddCommand("!buh", blank)
	b.AddCommand("!test", blank)

	b.Start()
}
