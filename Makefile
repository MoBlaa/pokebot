PROJECT_NAME := "pokebot"
PKG := "gitlab.com/MoBlaa/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint bench

all: build

lint: dep
	@golint -set_exit_status ${PKG_LIST}

test:
	@go test --short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race --short ${PKG_LIST}

coverage: ## Generate global code coverage report
	@mkdir -p cover
	@go test -coverprofile=cover/coverage.out ${PKG_LIST}
	@go tool cover -html=cover/coverage.out -o cover/coverage.html
	@go tool cover -func cover/coverage.out

dep: ## Get the dependencies
	@go get -v -d ./...
	@go get golang.org/x/lint/golint

build: dep ## Build the binary file
	GOOS=linux GOARCH=amd64 go build -i -o bin/${PROJECT_NAME} -v cmd/pokebot/main.go

build-win: dep ## Build the binary file for windows
	GOOS=windows GOARCH=amd64 CGO_ENABLED=1 go build -o bin/${PROJECT_NAME}.exe -v cmd/pokebot/main.go

pull-ui:
	@rm -rf static
	@mkdir -p static
	# Download latest packaged ui
	@curl -L -o static/ui.zip https://gitlab.com/MoBlaa/bui/-/jobs/artifacts/master/download?job=build
	# Unzip the contents
	@unzip -j static/ui.zip 'dist/bui/*' -d static
	# Remove obsolete zip file
	@rm static/ui.zip

run: dep build package-latest ## Start the bot
	./dist/linux/pokebot

clean: ## Remove previous build
	@rm -fr bin
	@rm -fr dist

package-linux: build pull-ui ## Builds backend bot, downloads latest ui and zips them for deployment
	# Create folder structure
	@mkdir -p dist/linux
	# Copy binary and config
	@cp -r bin/pokebot dist/linux/pokebot
	@cp -r configs dist/linux
	@cp -r static dist/linux

zip-linux: package-linux
	cd dist/linux; mv configs/env.template configs/.env; zip -o -r pokebot-linux-amd64.zip *; cp pokebot-linux-amd64.zip ../../

package-win: build-win pull-ui ## Build with windows binaries
	# Create folder structure
	@mkdir -p dist/win
	# Copy binary, config and static files
	@cp -r bin/pokebot.exe dist/win/pokebot.exe
	@cp -r configs dist/win
	@cp -r static dist/win

zip-win: package-linux
	cd dist/win; mv configs/env.template configs/.env; zip -o -r pokebot-win-amd64.zip *; cp pokebot-win-amd64.zip ../../