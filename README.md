## Owner of this bot

__<Fill in your username>__
 
(developed by __Mo_Blaa__)

## Description

This bot is using the [PokeAPI](https://pokeapi.co/) to show informations about pokémon and pokémon types.

# Commands

- !type <pokemon name>: Return the types of a pokémon. Name can be english, german with additons like: `charizard-mega-y`
- !weakness <pokemon type>: Return the type weaknesses of a pokémon type.
- !en <german pokemon name>: Return the english name of the pokémon type.
- !eff <pokemon name>: Return the type effectivenesses (0x, 1/4x, 1/2x, 2x, 4x Damage - 1x Damage is not included as it would heavily increase the message size.

__May be subject of change__

## How to add/remove the Bot to/from the Channel

__This account is dedicated to the channel <Fill in your username>__ and will not support adding other channels to its functionality.

To run the bot on your own channel please register your own account and follow these steps:

1. The required files can directly be downloaded [here for windows](https://gitlab.com/MoBlaa/pokebot/-/jobs/artifacts/master/download?job=build-windows) and [here for linux](https://gitlab.com/MoBlaa/pokebot/-/jobs/artifacts/master/download?job=build-windows). The actual code is hostet on Gitlab.com [here](https://gitlab.com/MoBlaa/pokebot).
3. After downloading the `.zip` archives extract them somewhere and open the `configs` folder. Edit the `env.template` file and fill in the missing information.
4. After editing the file rename it to `.env`.
5. Run the `pokebot` (linux) `pokebot.exe` (windows) executable. A window showing the login flow and chat messages with some additional information will be shown here. While the window is opened the bot is running.

If any problems or questions occur please contact the developer (__Mo_Blaa__) through a Twitch Whisper.